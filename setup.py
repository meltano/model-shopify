from setuptools import setup, find_packages

setup(
    name="model-shopify",
    version="0.2",
    description="Meltano .m5o models for data fetched using tap-shopify",
    packages=find_packages(),
    package_data={"models": ["**/*.m5o", "*.m5o"]},
    install_requires=[],
)
