{
  version = 1
  sql_table_name = shopify_sales_stats_per_day
  name = shopify_sales_stats_per_day
  columns {
    processed_date {
      label = Processed Date
      description = Processed Date
      type = date
      sql = "{{table}}.processed_date"
    }
    currency {
      label = Currency
      description = "The three-letter code (ISO 4217 format) for the shop currency."
      type = string
      sql = "{{table}}.currency"
    }
  }
  timeframes {
    processed_date {
      include "mixins/_timeframe.proto.m5o"
      label = Processed Date
      type = date
      sql = "{{table}}.processed_date"
    }
  }
  aggregates {
    total_orders {
      label = Orders
      type = sum
      sql = "{{table}}.order_count"
    }
    total_gross_sales {
      label = "Gross Sales"
      description = "Equates to product price x quantity (before taxes, shipping, discounts, and returns) for a collection of sales. Canceled, pending, and unpaid orders are included. Test and deleted orders are not included."
      type = sum
      sql = "{{table}}.gross_sales"
    }
    total_discounts {
      label = "Discounts"
      description = "Equates to line item discount + order level discount share for a collection of sales."
      type = sum
      sql = "{{table}}.discounts"
    }
    total_returns {
      label = "Returns"
      description = "The value of goods returned by a customer."
      type = sum
      sql = "{{table}}.returns"
    }
    total_net_sales {
      label = "Net Sales"
      description = "Equates to gross sales - discounts - returns."
      type = sum
      sql = "{{table}}.net_sales"
    }
    total_shipping {
      label = "Shipping"
      description = "Equates to shipping charges - shipping discounts - refunded shipping amounts."
      type = sum
      sql = "{{table}}.shipping"
    }
    total_taxes {
      label = "Tax"
      description = "The total amount of taxes based on the orders."
      type = sum
      sql = "{{table}}.taxes"
    }
    total_sales {
      label = "Total sales"
      description = "Equates to gross sales - discounts - returns + taxes + shipping charges."
      type = sum
      sql = "{{table}}.total_sales"
    }
  }
}
